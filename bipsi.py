#!/usr/bin/python3

#ZDROJE INFORMACE:
# https://realpython.com/python-sockets/
# https://realpython.com/intro-to-python-threading/
# https://www.fit-wiki.cz/%C5%A1kola/p%C5%99edm%C4%9Bty/bi-psi
# https://docs.python.org/3/library/threading.html?highlight=threading#module-threading
# https://docs.python.org/3/library/socket.html
# http://vyuka.ookami.cz/@CVUT_2018+19-LS_BI-PYT/
#https://stackoverflow.com/questions/42612002/python-sockets-error-typeerror-a-bytes-like-object-is-required-not-str-with?noredirect=1&lq=1
#https://www.reddit.com/r/learnprogramming/comments/8cfjbk/valueerror_invalid_literal_for_int_with_base_10/
#https://stackoverflow.com/questions/10880813/typeerror-sequence-item-0-expected-string-int-found

import socket
import sys
import threading
import logging
import os
logging.basicConfig(level = logging.INFO, filemode='w', filename = "/home/alexandr/scripts/python/loggerpyt")

#PROGRAM CONSTANTS - SERVER
SERVER_CONFIRMATION = str()
SERVER_MOVE = '102 MOVE\a\b'
SERVER_TURN_LEFT = '103 TURN LEFT\a\b'
SERVER_TURN_RIGHT = '104 TURN RIGHT\a\b'
SERVER_PICK_UP = '105 GET MESSAGE\a\b'
SERVER_LOGOUT = '106 LOGOUT\a\b'
SERVER_OK = '200 OK\a\b'
SERVER_LOGIN_FAILED = '300 LOGIN FAILED\a\b'
SERVER_SYNTAX_ERROR = '301 SYNTAX ERROR\a\b'
SERVER_LOGIC_ERROR = '302 LOGIC ERROR\a\b'
#PROGRAM CONSTANTS - CLIENT
CLIENT_RECHARGING = 'RECHARGING\a\b'
CLIENT_FULL_POWER = 'FULL POWER\a\b'
CLIENT_USERNAME_LENGTH = 12
CLIENT_CONFIRMATION_LENGTH = 7
CLIENT_OK_LENGTH = 12
CLIENT_RECHARGING_LENGTH = 12
CLIENT_FULL_POWER_LENGTH = 12
CLIENT_MESSAGE_LENGTH = 100
TIMEOUT_RECHARGING = 5
#PROGRAM CONSTANTS - GENERAL
TIMEOUT = 1
#HOST = '192.168.0.178'
HOST = '127.0.0.1'
PORT = 65432
SERVER_KEY = 54621
CLIENT_KEY = 45328
GOAL_X = 2
GOAL_Y = 2

class Server:

	def __init__(self):
		#the server initializes itself with a list of threads.
		self.threads = list()

	def establish_connection(self, socket):
		#Opens a thread with a socket in it. The socket runs and expects data.
		conn, addr = socket.accept()
		print("Connected from: ", addr)
		#now we have successfully established connection on this socket.
		#Create a thread named thread1, which acts as a container for threaded_connection function passing arguments socket, conn, addr
		thread1 = threading.Thread(target = self.threaded_connection, args = (socket, conn, addr))
		#appending the thread to a list
		self.threads.append(thread1)
		#starting the thread
		thread1.start()

	def threaded_connection(self, socket, conn, addr):
		#this is a socket connection inside a thread.
		cycle_counter = 0
		cycle_counter2 = 0
		last_message_dig = False
		stage = 1
		moved = 1
		left_or_right = 1
		turning_stage = 0
		list_data = list()
		data_queue = list()
		on_queue = False
		leftover_data = list()
		asshole_design = False

		while True:
			if data_queue:
				for x in range(len(data_queue)):
					logging.info("data_queue [%s]: %s", x, data_queue[x])
				data = data_queue[0].encode()
				on_queue = True
				logging.info("queue 0: %s", data_queue[0])
				del data_queue[0]
				if not data_queue:
					if leftover_data:
						list_data.append(leftover_data)
			elif (not asshole_design):
				on_queue = False
				if (not leftover_data):
					data = conn.recv(1024)
				else:
					data = leftover_data
					data = data.encode()
					leftover_data = ''
				print("Incoming: ", data)
			asshole_design = False
			if ((on_queue == False) and (multiple_messages(data))):
				data_queue = cut_data(data)
				if (data_queue[len(data_queue) - 1] == ''):
					data_queue.pop()
				logging.info("cutting..")
				continue
			if ((on_queue == False) and (has_ended(data) == False)):
				list_data.append(data)
				logging.info("appending..")
				continue
			if list_data:
				logging.info("it's not empty!")
				list_data.append(data)
				data = concat_to_string(list_data)
				if (validate_asshole_data(data)):
					data, leftover_data = asshole_cut(data)
					data[0] = data[0].encode()
					data = concat_to_string(data)
					data = data.encode()
					logging.info("data after asshole design: [%s]: %s", 0, data[0])
					list_data.clear()
					asshole_design = True
					continue
				data = data.encode()
				logging.info("concatenated string: %s", data)
				list_data.clear()
			#just connected and received a name
			if (cycle_counter == 0):
				chars = list(data)
				normalize_message(chars)
				server_hash = 0
				for char in chars:
					server_hash += int(char)
				result_hash = (server_hash * 1000) % 65536
				confirmation_hash = (result_hash + SERVER_KEY) % 65536
				#set SERVER_CONFIRMATION message to the confirmation hash along with adding the \a\b at the end
				SERVER_CONFIRMATION = edit_mesg(confirmation_hash)
				#sending it to the connected client
				#not byte-like error solved..?
				conn.sendall(SERVER_CONFIRMATION.encode())
				cycle_counter += 1
				continue
			#if the client authenticated me, I have to authenticate him as well
			if (cycle_counter == 1):
				if compare_hashes(data, result_hash):
					conn.sendall(SERVER_OK.encode())
					cycle_counter += 1
					#creating an instance for the client
					new_client1 = Client()
					logging.info("hashes OK")
				else:
					conn.sendall(SERVER_LOGIN_FAILED.encode())
					conn.close()
					break
			#starter configuration. this is only a two time sequence.
			#we get the client's coordinates and validate if he's moved, if not then he's instructed to move again.
			#if he moved successfully and everything works out, he now has traversed two blocks meaning he has a previous and a current coordinate x,y
			if (cycle_counter == 3 or cycle_counter == 4):
				new_client1.coordinate_x, new_client1.coordinate_y = new_client1.extract_coordinates(data)
				logging.info("coords x,y: %s %s", new_client1.coordinate_x, new_client1.coordinate_y)
				if not new_client1.validate_movement():
					conn.sendall(SERVER_MOVE.encode())
					continue
				if (cycle_counter == 4):
					cycle_counter += 1
					logging.info("moved twice OK")
				if (cycle_counter == 3):
					new_client1.previous_coordinate_x = new_client1.coordinate_x
					new_client1.previous_coordinate_y = new_client1.coordinate_y
			if (cycle_counter == 2 or cycle_counter == 3):
				conn.sendall(SERVER_MOVE.encode())
				cycle_counter += 1
				continue
			if ((cycle_counter > 4) and (new_client1.direction is None) and (new_client1.goal_direction is None)):
				new_client1.get_direction()
				new_client1.get_goal_direction()
			if ((new_client1.goal_direction is not None) and (new_client1.direction is not None)):
				if (new_client1.direction != new_client1.goal_direction):
					if (new_client1.get_turning_side() == 'LEFT'):
						conn.sendall(SERVER_TURN_LEFT.encode())
						new_client1.direction = new_client1.directions[new_client1.directions.index(new_client1.direction) - 1]
					else:
						conn.sendall(SERVER_TURN_RIGHT.encode())
						if (new_client1.directions.index(new_client1.direction) == 3):
							new_client1.direction = new_client1.directions[0]
						else:
							new_client1.direction = new_client1.directions[new_client1.directions.index(new_client1.direction) + 1]
					continue
			#X COORDINATE TO 2
			if ((new_client1.direction == new_client1.goal_direction) and (int(new_client1.coordinate_x) != GOAL_X)):
				if (cycle_counter2 > 0):
					new_client1.coordinate_x, new_client1.coordinate_y = new_client1.extract_coordinates(data)
				if not new_client1.validate_movement():
					conn.sendall(SERVER_MOVE.encode())
					continue
				cycle_counter2 += 1
				new_client1.previous_coordinate_x = new_client1.coordinate_x
				new_client1.previous_coordinate_y = new_client1.coordinate_y
				if ((int(new_client1.coordinate_x) == GOAL_X) and (int(new_client1.coordinate_y) == GOAL_Y)):
					conn.sendall(SERVER_PICK_UP.encode())
					new_client1.entered_22 = True
					last_message_dig = True
					continue
				if (int(new_client1.coordinate_x) == GOAL_X):
					new_client1.get_goal_direction()
					if (new_client1.get_turning_side() == 'LEFT'):
						conn.sendall(SERVER_TURN_LEFT.encode())
						new_client1.direction = new_client1.directions[new_client1.directions.index(new_client1.direction) - 1]
					else:
						conn.sendall(SERVER_TURN_RIGHT.encode())
						if (new_client1.directions.index(new_client1.direction) == 3):
							new_client1.direction = new_client1.directions[0]
						else:
							new_client1.direction = new_client1.directions[new_client1.directions.index(new_client1.direction) + 1]
					continue
				new_client1.move(conn)
				continue
			#Y COORDINATE TO 2
			if ((new_client1.direction == new_client1.goal_direction) and (int(new_client1.coordinate_y) != GOAL_Y)):
				new_client1.coordinate_x, new_client1.coordinate_y = new_client1.extract_coordinates(data)
				logging.info("working on Y")
				if not new_client1.validate_movement():
					conn.sendall(SERVER_MOVE.encode())
					continue
				new_client1.previous_coordinate_x = new_client1.coordinate_x
				new_client1.previous_coordinate_y = new_client1.coordinate_y
				if (int(new_client1.coordinate_y) == GOAL_Y):
					conn.sendall(SERVER_PICK_UP.encode())
					last_message_dig = True
					new_client1.entered_22 = True
					continue
				new_client1.move(conn)
				continue
			#GOT TO [2;2]
			if ((int(new_client1.coordinate_x) == GOAL_X) and (int(new_client1.coordinate_y) == GOAL_Y) and (last_message_dig == False)):
				logging.info("got here lol")
				conn.sendall(SERVER_PICK_UP.encode())
				last_message_dig = True
				new_client1.entered_22 = True
				continue
			if (new_client1.entered_22 == True):
				if (last_message_dig == True):
					the_secret_message = extract_intelligence(data)
					if ((the_secret_message != '') and (the_secret_message != CLIENT_RECHARGING)):
						conn.sendall(SERVER_LOGOUT.encode())
						conn.close()
						break
				if (moved % 5 != 0):
					if (stage == 0):
						new_client1.coordinate_x, new_client1.coordinate_y = new_client1.extract_coordinates(data)
						if (not validate_movement()):
							new_client1.move(conn)
							last_message_dig = False
							continue
						moved += 1
						conn.sendall(SERVER_PICK_UP.encode())
						last_message_dig = True
						stage = 1
						continue
					if(stage == 1):
						new_client1.move(conn)
						last_message_dig = False
						stage = 0
						continue
				else:
					#turn, move down and turn again, set moved to 0, set stage 0
					if (turning_stage == 0):
						#turning once
						if (left_or_right % 2 != 0):
							conn.sendall(SERVER_TURN_LEFT.encode())
							turning_stage = 1
							continue
						if (left_or_right % 2 == 0):
							conn.sendall(SERVER_TURN_RIGHT.encode())
							turning_stage = 1
							continue
					if (turning_stage == 1):
						#move
						new_client1.move(conn)
						last_message_dig = False
						turning_stage = 2
						continue
					if (turning_stage == 2):
						#turn again, set moved to 0 and stage to 0
						if (not validate_movement()):
							new_client1.move(conn)
							last_message_dig = False
							continue
						if (left_or_right % 2 != 0):
							conn.sendall(SERVER_TURN_LEFT.encode)
						if (left_or_right % 2 == 0):
							conn.sendall(SERVER_TURN_RIGHT.encode())
						moved = 0
						stage = 0
						left_or_right += 1



	def validate_message(message):
		#server validates a received message.
		#returns true or false and along with the latter the error code
		test4

class Client:

	def __init__(self):
		#the client(robot) initiates itself.
		self.directions = ("LEFT", "UP", "RIGHT", "DOWN")
		self.direction = None
		self.previous_coordinate_x = None
		self.previous_coordinate_y = None
		self.coordinate_x = int()
		self.coordinate_y = int()
		self.entered_22 = False
		self.goal_direction = None

	#extracts coordinates from the CLIENT_OK message
	def extract_coordinates(self, message):
		coords = list(message)
		coords.pop()
		coords.pop()
		for x in range(len(coords)):
			coords[x] = chr(coords[x])
		coords = ''.join(str(value) for value in coords)
		coord_x, coord_y = (coords.lstrip('OK ')).split()
		return coord_x, coord_y

	def move(self, conn):
		conn.sendall(SERVER_MOVE.encode())
		self.previous_coordinate_x = self.coordinate_x
		self.previous_coordinate_y = self.coordinate_y

	def get_turning_side(self):
		if ((self.goal_direction == 'UP') and (self.direction == 'RIGHT')):
			return 'LEFT'
		elif ((self.goal_direction == 'DOWN') and (self.direction == 'LEFT')):
			return 'LEFT'
		elif ((self.goal_direction == 'LEFT') and (self.direction == 'UP')):
			return LEFT
		elif ((self.goal_direction == 'RIGHT') and (self.direction == 'DOWN')):
			return 'LEFT'
		else:
			return 'RIGHT'

	def get_direction(self):
		if (int(self.coordinate_x) > int(self.previous_coordinate_x)):
			self.direction = self.directions[2]
		elif (int(self.coordinate_x) < int(self.previous_coordinate_x)):
			self.direction = self.directions[0]
		elif (int(self.coordinate_y) > int(self.previous_coordinate_y)):
			self.direction = self.directions[1]
		elif (int(self.coordinate_y) < int(self.previous_coordinate_y)):
			self.direction = self.directions[3]

	def get_goal_direction(self):
		if (int(self.coordinate_x) != 2):
			if (int(self.coordinate_x) > 2):
				self.goal_direction = 'LEFT'
			elif (int(self.coordinate_x) < 2):
				self.goal_direction = 'RIGHT'
		elif (int(self.coordinate_y) != 2):
			if (int(self.coordinate_y) > 2):
				self.goal_direction = 'DOWN'
			elif (int(self.coordinate_y) < 2):
				self.goal_direction = 'UP'

	#validates the client's movement
	def validate_movement(self):
		if (self.previous_coordinate_x is not None):
			if ((int(self.previous_coordinate_x) == int(self.coordinate_x)) and (int(self.previous_coordinate_y) == int(self.coordinate_y))):
				return False
			else:
				return True
		else:
			return True

def extract_intelligence(message):
	intelligence = list(message)
	intelligence.pop()
	intelligence.pop()
	if (len(intelligence) == 0):
		return ''
	for x in range(len(intelligence)):
			intelligence[x] = chr(intelligence[x])
	intelligence = ''.join(str(value) for value in intelligence)
	return intelligence

def asshole_cut(asshole_data):
	#data = data.decode()
	asshole_data = asshole_data.replace('\x07\x08', '\x07\x08@')
	asshole_data = asshole_data.split('@')
	leftover_stuff = asshole_data[len(asshole_data) - 1]
	del asshole_data[-1]
	return asshole_data, leftover_stuff

def has_ended(data):
	data = data.decode()
	if (('\a\b' in data) or ('\x07\x08' in data)):
		return True
	else:
		return False

def concat_to_string(list_data):
	for x in range(len(list_data)):
			list_data[x] = list_data[x].decode()
			#list_data[x] = chr(list_data[x])
	c_data = ''.join(str(value) for value in list_data)
	return c_data

def validate_asshole_data(data):
	if ((not data.endswith('\x07\x08')) and ('\x07\x08' in data)):
		return True
	else:
		return False
def multiple_messages(data):
	logging.info("the type : %s", type(data))
	data = data.decode()
	msg_count = data.count('\x07\x08')
	logging.info("count: %s", msg_count)
	if (msg_count > 1):
		return True
	else:
		return False

#if data contains multiple messages, push them into a list and stall receiving data by returning True to continue in the main loop.
#new data becomes each message sequentially in the list.
def cut_data(data):
	data = data.decode()
	data_queue = data.replace('\x07\x08', '\x07\x08@')
	data_queue = data_queue.split('@')
	return data_queue

#compares my hash with the client's hash
def compare_hashes(client_hash, result_hash):
	client_hash = list(client_hash)
	client_hash = normalize_message(client_hash)
	for x in range(len(client_hash)):
		client_hash[x] = chr(client_hash[x])
	client_hash = ''.join(str(value) for value in client_hash)
	client_hash = int(client_hash)
	confirmation_hash2 = (client_hash + 65536 - CLIENT_KEY) % 65536
	if confirmation_hash2 == result_hash:
		return True
	else:
		return False 
#normalizes an incoming message by removing \a\b from the end
def normalize_message(message):
	del message[-1]
	del message[-1]
	return message

#edits a string to be suitable for transmission
def edit_mesg(message):
	message = str(message)
	message += "\a\b"
	return message

def main():
	#create server instance
	new_server1 = Server()
	#create socket to listen for ipv4 interfaces
	socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#bind the socket to host ip and port
	socket1.bind((HOST, PORT))
	#listen on the socket
	socket1.listen()
	#accept the connection on the socket
	#working with object connection and ip address coming from the client (socket.accept will let me receive them)
	while True:
		new_server1.establish_connection(socket1)

if __name__ == '__main__':
	main()